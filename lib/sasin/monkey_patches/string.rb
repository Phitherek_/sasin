# frozen_string_literal: true

require_relative '../converter'
class String
  def to_sasin
    Sasin::Converter.convert(self)
  end
end
