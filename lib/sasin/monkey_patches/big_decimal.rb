# frozen_string_literal: true

require_relative '../converter'
class BigDecimal
  def to_sasin
    Sasin::Converter.convert(self)
  end
end
