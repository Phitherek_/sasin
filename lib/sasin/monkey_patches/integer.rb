# frozen_string_literal: true

require_relative '../converter'
class Integer
  def to_sasin
    Sasin::Converter.convert(self)
  end
end
