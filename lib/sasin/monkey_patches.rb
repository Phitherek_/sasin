# frozen_string_literal: true

require_relative 'monkey_patches/string'
require_relative 'monkey_patches/integer'
require_relative 'monkey_patches/float'
require_relative 'monkey_patches/big_decimal'
