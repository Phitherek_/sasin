# frozen_string_literal: true

require 'bigdecimal'
module Sasin
  class Converter
    RATE = 70_000_000 # This is 1 sasin in PLN, but for simplicity we adopt it as a universal rate
    def self.convert(param)
      coerced_param = if param.is_a?(BigDecimal)
                        param
                      else
                        BigDecimal(param.to_s)
                      end
      (coerced_param / RATE).to_f.round(25)
    end
  end
end
