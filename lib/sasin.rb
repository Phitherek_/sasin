# frozen_string_literal: true

require_relative 'sasin/version'
require_relative 'sasin/converter'

module Sasin
  class Error < StandardError; end
end
