# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/sasin/converter'

RSpec.describe Sasin::Converter do
  describe '#convert' do
    it 'should properly convert to sasin' do
      expect(described_class.convert(1)).to eq(0.000000014285714286)
      expect(described_class.convert('1.5')).to eq(0.000000021428571428571429)
      expect(described_class.convert(1.5)).to eq(0.000000021428571428571429)
      expect(described_class.convert(BigDecimal('1.5'))).to eq(0.000000021428571428571429)
      expect(described_class.convert(-1)).to eq(-0.000000014285714286)
      expect(described_class.convert('-1.5')).to eq(-0.000000021428571428571429)
      expect(described_class.convert(-1.5)).to eq(-0.000000021428571428571429)
      expect(described_class.convert(BigDecimal('-1.5'))).to eq(-0.000000021428571428571429)
    end
  end
end
