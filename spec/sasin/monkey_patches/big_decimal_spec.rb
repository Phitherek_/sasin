# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/sasin/monkey_patches/big_decimal'

RSpec.describe BigDecimal do
  describe '#to_sasin' do
    it 'should properly convert to sasin' do
      expect(BigDecimal('1.5').to_sasin).to eq(0.000000021428571428571429)
      expect(BigDecimal('-1.5').to_sasin).to eq(-0.000000021428571428571429)
    end
  end
end
