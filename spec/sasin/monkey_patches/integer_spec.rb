# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/sasin/monkey_patches/integer'

RSpec.describe Integer do
  describe '#to_sasin' do
    it 'should properly convert to sasin' do
      expect(1.to_sasin).to eq(0.000000014285714286)
      expect(-1.to_sasin).to eq(-0.000000014285714286)
    end
  end
end
