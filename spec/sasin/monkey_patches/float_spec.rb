# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/sasin/monkey_patches/float'

RSpec.describe Float do
  describe '#to_sasin' do
    it 'should properly convert to sasin' do
      expect(1.5.to_sasin).to eq(0.000000021428571428571429)
      expect(-1.5.to_sasin).to eq(-0.000000021428571428571429)
    end
  end
end
