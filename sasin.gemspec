# frozen_string_literal: true

require_relative 'lib/sasin/version'

Gem::Specification.new do |spec|
  spec.name          = 'sasin'
  spec.version       = Sasin::VERSION
  spec.authors       = ['Phitherek_']
  spec.email         = ['phitherek@gmail.com']

  spec.summary       = 'A converter to an universal sasin unit'
  spec.description   = 'This is a Ruby implementation of a converter to a universal sasin unit'
  spec.homepage      = 'https://gitlab.com/Phitherek_/sasin'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.4.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/Phitherek_/sasin'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
