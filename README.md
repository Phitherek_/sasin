# Sasin

This is a Ruby gem that implements a converter to a universal sasin unit.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'sasin'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install sasin

## Usage

### Regular

```ruby
require 'sasin'

Sasin::Converter.convert(1) # => 0.0000000142857142860000008
```

### With monkey patches

```ruby
require 'sasin/monkey_patches'

1.to_sasin # => 0.0000000142857142860000008
```

Supports String, Integer, Float and BigDecimal.

### Binary

Run `sasin` and enter input to get the result.

Run `sasin INPUT` to get the result.

## Development

Run `rake spec` to run the tests.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/Phitherek_/sasin.


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Disclaimer

Yes, this gem is kind of a joke. It refers to a Polish minister of assets Jacek Sasin that spent a 70 million of Polish zloty on an election that didn't finally happen. It was also kind of inspired by a Python package of the same name.